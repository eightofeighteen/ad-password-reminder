Please remember to change it before it expires or you may experience problems in logging into your computer and accessing your e-mail or information.
If you are away from the office, you may contact your local helpdesk for assistance in changing your password.

Yours sincerely
An Automated Mailer

P.S. Please do not reply to this message.  Replies are not monitored by anybody.