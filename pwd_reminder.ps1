﻿$endl = "`n";

$sites = Get-Content "sites.txt";  # Read list of site DNs from file.
$count = 0;

# Copy each user account with a mailbox from DNs in $sites and adds them to $data.
ForEach ($site in $sites) {
    Echo $site;
    csvde -d $site -r "(&(objectCategory=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(userAccountControl:1.2.840.113556.1.4.803:=65536))(homeMDB=*))" -l "samAccountName,givenName,sn,pwdLastSet,mail" -s HODC01:3268 -f out.csv;
    if ($count -eq 0) {
        $data = Import-CSV out.csv;
    } else {
        $data += Import-CSV out.csv;
    }
    $count++;
}

Remove-Item out.csv -Force;
$now = Get-Date;
$period = 30;
$notify = 7;

#$notifyList = $data;
#$notifyList.clear();

# Generates a mail message using user's name and a message from test file "static_message.txt".
function sendMessage($recipient, $firstname, $lastName, $cut, $mailServer) {
    $message = ("Dear " + $firstname + " " + $lastName + " (" + $recipient + ")" + $endl + $endl);
    $message += ("Our records indicate that your Windows password is due to expire in " + $cut + " days." + $endl);
    $static_message = Get-Content "static_message.txt";
    #$message += $static_message + $endl;
    ForEach ($line in $static_message) {
        $message += $line;
        $message += $endl;
    }
    $message;
    send-mailmessage -to $recipient -subject "Windows Password Expiration Reminder" -from "noreply@illovo.co.za" -body $message -smtpserver $mailServer;
}

# Searches $data for users who's passwords are expiring in 7 days, and e-mails them.
ForEach ($user in $data) {
    $cut = [datetime]::FromFileTime($user.pwdLastSet).AddDays($period).AddDays(-$notify);
    if ($now -ge ($cut)) {
        if ($now -lt ($cut.AddDays(1))) {
            #($user.givenName + " " + $user.sn + " " + [datetime]::FromFileTime($user.pwdLastSet) + " " + [datetime]::FromFileTime($user.pwdLastSet).AddDays($period).Add(-$now).Day);
            #sendMessage $user.mail $user.givenName $user.sn ([datetime]::FromFileTime($user.pwdLastSet).AddDays($period).Add(-$now)).Day "exchhub.za.illovo.net";
            sendMessage "spurnell@illovo.co.za" $user.givenName $user.sn ([datetime]::FromFileTime($user.pwdLastSet).AddDays($period).Add(-$now)).Day "exchhub.za.illovo.net";
        }
    }
}
