﻿csvde -d "OU=Head Office,DC=za,DC=illovo,DC=net" -r "(&(objectCategory=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(userAccountControl:1.2.840.113556.1.4.803:=65536))(homeMDB=*))" -l "samAccountName,givenName,sn,pwdLastSet,mail" -s HODC01:3268 -f out.csv;
$data = Import-CSV out.csv;
$now = Get-Date;
$period = 42;
$notify = 7;

#$notifyList = $data;
#$notifyList.clear();


ForEach ($user in $data) {
    $cut = [datetime]::FromFileTime($user.pwdLastSet).AddDays($period).AddDays(-$notify);
    if ($now -ge $cut) {
        $notifyList += $user;
    }
}
#$notifyList | ft;

ForEach ($user in $notifyList) {
    $out = ($user.givenName + " " + $user.mail + " " + [datetime]::FromFileTime($user.pwdLastSet).AddDays($period).Subtract($now).Days);
    $out;
}